---
title: First steps in Python-Programming
layout: event
organizers:
  - erxleben
lecturers:
  - TBA
type:   workshop
start:
    date:   "2021-04-19"
end:
    date:   "2021-04-19"
registration_link: https://events.hifis.net/event/93/
location:
    campus: Online
fully_booked_out: false
registration_period:
    from:   "2021-04-05"
    to:     "2021-04-15"
excerpt:
    "An Introduction for scientists and PhD. students to programming using the
     language Python. No prior experience required."
---

## Goal

Enable the participants to write their own scripts in Python to automatically
evaluate data and solve recurring or labourious tasks by automation.

## Content

The course will introduce basic concepts of the language.
Emphasis will be put on live coding (i.e. learners write their code along with
the instructors) and overcoming the initial learning hurdles together.
Hands-on exercises give the opportunity to test the newly acquired knowledge.

## Requirements

Neither prior knowledge nor experience in the area is needed.
Participants are asked to bring their own computer on which they can install
software.
Detailed instructions will be made available on the workshop website.


