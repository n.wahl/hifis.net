---
title: Introduction to Snakemake
layout: event
organizers:
  - Steinbach, Peter
lecturers:
  - Steinbach, Peter
type:   workshop
start:
    date:   "2019-12-11"
    time:   "14:00"
end:
    time:   "17:00"
location:
    campus: hzdr
    room:   "104/215"
fully_booked_out: true
registration_link: https://schedule.tii.tu-dresden.de/event/14/
registration_period:
    from: "2019-12-01"
    to:   "2019-12-11"
excerpt: "This short course is meant to teach participants the automation
of pipelines. For this, the snakemake workflow engine is introduced."
---

## Content

This short course is meant to teach participants the automation of pipelines.
For this, the snakemake workflow engine is introduced.

It will be presented how to write recipes with this engine which are independent of
the programming language used.
Further, the capabilities of snakemake to parallelize these workflows are
demonstrated.
To conclude, a custom workflow will then be submitted to the cluster to
run on multiple machines.

The course will be fully interactive.

## Prerequesits

Users will work on hemera and are expected be sufficiently introduced to
the slurm scheduling system.

{{ page.path }}
