---
title: Newsletters
title_image: sign-pen-business-document-48148.jpg
layout: default
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
excerpt:
    HIFIS Newsletters
---

HIFIS Newsletters will be published quarterly from July 2022 onwards.
Find all published newsletters here in HTML and PDF formats.

Feel free to distribute the links to everyone who might be interested.

## Don't want to miss the next newsletter?

[**Subscribe here**](https://lists.desy.de/sympa/subscribe/hifis-newsletter) to receive all further newsletters automatically. Spread the word and share the link. Thank you!

## Published Newsletters so far:

- July 2022: [HTML]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.html %}), [PDF]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.pdf %})
