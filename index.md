---
title: HIFIS - Helmholtz Federated IT Services
title_image: default
layout: frontpage
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
excerpt:
  "HIFIS aims to provide excellent, seamlessly accessible IT services for the
  whole Helmholtz Association."
---
{% comment %}
  This markdown file triggers the generation of the frontpage.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
