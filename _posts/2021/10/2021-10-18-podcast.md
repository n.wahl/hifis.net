---
title: "Helmholtz Resonator Podcast on HIFIS"
title_image: joseph-chan-E4MFo1bQY7I-unsplash.jpg
date: 2021-10-18
authors:
  - jandt
  - englaender
layout: blogpost
categories:
  - News
excerpt: >
    HIFIS has been given opportunity to present its scope and status at the
    <a href="https://resonator-podcast.de/">
    Helmholtz Resonator Podcast</a>.
    This free Podcast focuses on all kinds of research and related activities in Helmholtz.

---

# The Podcast

{{ page.excerpt }}

The podcast appears monthly and is in German.

<i class="fas fa-headphones"></i> Feel free to [tune in](https://resonator-podcast.de/2021/res172-hifis/) anytime! <i class="fas fa-headphones"></i>

#### Content of this podcast episode: All you want to know about HIFIS

In this episode of the podcast,
Uwe Jandt (HIFIS Coordination) and
Carina Haupt (HIFIS Software Services)
talk about cloud, education and consulting services
provided by HIFIS to the Helmholtz research community.

Further topics cover the fostering of good practices in Resarch Software Engineering (RSE),
open works yet to be done for HIFIS, as well as
the close relationship to the scientific Incubator platforms -- e.g. with respect to scientific metadata management --,
and many more!

#### Listen or Download

This interview on HIFIS can be [played or downloaded here](https://resonator-podcast.de/2021/res172-hifis/)
at the Resonator page.


## Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
